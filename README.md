# Doom - Corona Infection

![](doc/pics/jkcorona_pic1.png)
![](doc/pics/jkcorona_pic2.png)
![](doc/pics/jkcorona_pic3.png)

## General Description

Doom 2 map in UDMF format.

**Version 1.5**
**Release Date: 2021-04-13**

This map is created specifically for GZDoom and Brutal Doom. I didn't bother much about testing it without jumping and freelook, so consider this map to be designed for jumping and freelook.

With this map I wanted to take a plunge into the UDMF format. However this time I wanted it to be based on Doom2 to get more enemy types. Yet I decided to stay with the builtin textures only. For your convenience I've emdedded all Doom 1 textures directly into this WAD.

There's not much story behind this project except for a personal one. I contracted corona virus at the end of 2020. It was tough at first, but I managed to handle it at home without the need for any medical assistance. So that's where the map's title comes from. I thought about a map that looks like a typical Doom 1 tech style map with sections infected by a hellish virus. UDMF format with tones of great features was increadibly helpfull in bringing my ideas to life. It also allowed me to expand my map editting skills.

Have fun!

## Map Features

- New levels              : 1
- Sounds                  : No
- Music                   : No
- Graphics                : No
- Dehacked/BEX Patch      : No
- Demos                   : No
- Other                   : No
- Other files required    : No


*Play Information*

- Game                    : DOOM 2
- Map #                   : MAP1
- Single Player           : Designed for
- Cooperative 2-4 Player  : No
- Deathmatch 2-4 Player   : No
- Other game styles       : None
- Difficulty Settings     : Yes


*Construction*

- Base                    : New from scratch
- Build Time              : Around 2 months 
- Editor(s) used          : Ultimate Doom Builder
- Known Bugs              : Unknown
- May Not Run With        : Unknown
- Tested With             : GZDoom 4.2.2, Zandronum 3.0, Brutal Doom 21 RC1 FIX

*Special thanks*

Special thanks go to the Doomworld.com forum members for playing through the
previous versions of the map and for their remarks.

## Links

[Author Fan Page](https://www.facebook.com/World-Reloaded-107897790984291)

[Doomworld.com map thread](https://www.doomworld.com/forum/topic/120850-new-map-corona-infection-map-for-doom-2/)


